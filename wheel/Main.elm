module Main exposing (..)

import Visualization.Shape as Shape exposing (defaultPieConfig)
import Array exposing (Array)
import Svg exposing (Svg, svg, g, path, text_, text)
import Svg.Attributes as SvgAttr exposing (transform, d, style, dy, width, height, textAnchor)
import Html exposing (Html, div, input, label, br, h2, button, img)
import Html.Attributes as HtmlAttr exposing (type_, value, step, class, id, style, src)
import Html.Events exposing (onInput, onClick)
import Animation exposing (px, turn, percent)
import Data exposing (..)
import Types exposing (..)


type alias Model =
    { config : ChartConfig
    , players : Players
    , rotateStyle : Animation.State
    }


type Msg
    = RemovePlayer
    | SpinWheel
    | Animate Animation.Msg


removePlayer : Players -> Players
removePlayer players =
    List.drop 5 players


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RemovePlayer ->
            ( { model | players = removePlayer model.players }, Cmd.none )

        SpinWheel ->
            ( { model
                | rotateStyle =
                    Animation.interrupt
                        [ Animation.to
                            [ Animation.rotate (turn 1)
                            ]
                        , Animation.set
                            [ Animation.rotate (turn 0) ]
                        ]
                        model.rotateStyle
              }
            , Cmd.none
            )

        Animate animMsg ->
            ( { model
                | rotateStyle = Animation.update animMsg model.rotateStyle
              }
            , Cmd.none
            )


view : Model -> Html Msg
view model =
    div [ class "wrap" ]
        [ div [ HtmlAttr.style [ ( "margin-bottom", "100px" ) ] ]
            [ button [ onClick RemovePlayer ]
                [ text "Fjern en person" ]
            , br [] []
            , button [ onClick SpinWheel ] [ text "Spin it" ]
            ]
        , div [ id "container" ]
            [ div [ id "canvas" ] []
            , img [ src "images/arrow.png", id "arrow" ] []
            , div (Animation.render model.rotateStyle)
                [ div [ id "wheel", SvgAttr.style "justify-content: space-around" ]
                    [ drawChart model.config model.players
                    ]
                ]
            , img [ src "images/circle.png", id "circle" ] []
            ]
        ]


model : ( Model, Cmd Msg )
model =
    ( { config =
            { outerRadius = 300
            , innerRadius = 20
            , padAngle = 0
            , cornerRadius = 0
            , labelPosition = 220
            }
      , players = players
      , rotateStyle =
            Animation.style
                [ Animation.rotate (turn 0.0)
                ]
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Animation.subscription Animate [ model.rotateStyle ]


main : Program Never Model Msg
main =
    Html.program
        { init = model
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


screenWidth : Float
screenWidth =
    650


screenHeight : Float
screenHeight =
    700


radius : Float
radius =
    min screenWidth screenHeight / 2


drawChart : ChartConfig -> Players -> Svg msg
drawChart config model =
    let
        pieData =
            model
                |> List.map Tuple.second
                |> Shape.pie
                    { defaultPieConfig
                        | innerRadius = config.innerRadius
                        , outerRadius = config.outerRadius
                        , padAngle = config.padAngle
                        , cornerRadius = config.cornerRadius
                        , sortingFn = \_ _ -> EQ
                    }

        makeSlice index datum =
            path [ d (Shape.arc datum), SvgAttr.style ("fill:" ++ (Maybe.withDefault "#000" <| Array.get index colors) ++ "; stroke: #fff;") ] []

        makeLabel slice ( label, value ) =
            text_
                [ transform ("translate" ++ toString (Shape.centroid { slice | innerRadius = config.labelPosition, outerRadius = config.labelPosition }))
                , dy ".35em"
                , textAnchor "middle"
                ]
                [ text label ]
    in
        svg [ width (toString (radius * 2) ++ "px"), height (toString (radius * 2) ++ "px") ]
            [ g [ transform ("translate(" ++ toString radius ++ "," ++ toString radius ++ ")") ]
                [ g [] <| List.indexedMap makeSlice pieData
                , g [] <| List.map2 makeLabel pieData model
                ]
            ]
