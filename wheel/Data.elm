module Data exposing (..)

import Array exposing (Array)
import Types exposing (..)

players : Players
players =
    [ ( "Lillie", 1 )
    , ( "Twanda", 1 )
    , ( "Violet", 3 )
    , ( "Robbie", 3 )
    , ( "Shenna", 1 )
    , ( "Jan", 3 )
    , ( "Ja", 1 )
    , ( "Ian", 3 )
    , ( "Anibal", 3 )
    , ( "Bobette", 2 )
    , ( "Gwenda", 3 )
    , ( "Ethelene", 3 )
    , ( "Jaqueline", 2 )
    , ( "Jere", 3 )
    , ( "Barry", 1 )
    , ( "Tona", 3 )
    , ( "Chadwick", 2 )
    ]


colors : Array String
colors =
    Array.fromList
        [ "#F44336"
        ,"#9C27B0"
        ,"#E91E63"
        ,"#2196F3"
        ,"#673AB7"
        ,"#3F51B5"
        ,"#009688"
        ,"#4CAF50"
        ,"#8BC34A"
        ,"#FFEB3B"
        ,"#CDDC39"
        ,"#FFC107"
        ,"#03A9F4"
        ,"#FF9800"
        ,"#FF5722"
        ,"#795548"
        ,"#00BCD4"
        ]
